# NarupaAudioClient - Currently Windows Only

#### Clone gRPC
see [here](https://github.com/grpc/grpc/blob/master/BUILDING.md) - or just clone to a sensible location:
```
git clone --recursive -b ((New-Object System.Net.WebClient).DownloadString("https://grpc.io/release").Trim()) https://github.com/grpc/grpc
```
#### Build the bundled GrpcProject
* after cloning this repo, open GrpcProject\CMakeLists.txt for editing. 
* Edit line 14 to point to your gRPC directory:
```
SET(GRPC_DIR "<PATH/TO/GRPC/DIRECTORY>" CACHE PATH "Where gRPC is located")
```
* change into into GrpcProject\build and run cmake (your VS version or architecture may vary):
```
mkdir GrpcProject\build
cd GrpcProject\build
cmake -G "Visual Studio 15 2017 Win64" ..
```

* if you want to check so far then open the solution GrpcProject\build\NarupaClient.sln and check that it builds.

#### Building the Client / Audio Plugin
* Open AudioClient.jucer in the Projucer - if you don't have JUCE installed then get it [here](https://github.com/WeAreROLI/JUCE) - build and run the Projucer (located in the "extras" folder)
* In the Visual Studio exporter under Debug and Release edit the "Header Searh Paths" to match the gRPC path to these items on your system.
* Save and open the visual studio solution (click the visual studio icon).
* Add the GrpcProject as an aditional solution:
	- In visual studio right click on solution, add -> existing project
	- in the dropdown menu select "Solution Files (*.sln)"
	- navigate to and select GrpcProject\build\NarupaClient.sln 
* For the AudioClient_SharedCode, AudioClient_StandalonePlugin and AudioClient_UnityPlugin targets, right click references -> add references and tick:
 	- address_sorting
	- c-ares
	- gpr
	- gpr_unsecure
	- grpc++_unsecure
	- libprotobuf
	- ZERO_CHECK
	- zlibstatic

Build and cross those fingers

## Running the simulation 

Make sure [narupa-protocol](https://gitlab.com/intangiblerealities/narupa-protocol) is installed 

```
conda create -n narupa "python>3.6"
conda activate narupa
conda install -c irl -c omnia -c conda-forge narupa-server
conda install jupyter
```

Run the notebook:

```
cd graphenesynth/GrapheneSimulation
jupyter notebook
```

Click on `graphene_synthesis.ipynb` and run all the cells to start the simulation
and set up some sliders to control the parameters. 
