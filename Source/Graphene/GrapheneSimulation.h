#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "../NarupaClient/NarupaClient.h"

class GraphenePath
{
public:
	GraphenePath() = default;
	/** returns a list of atom indices that reference a ring of atoms path 1 is the innermost hexagon */
	static std::vector<int> getPath (int pathIndex); 
private:
};

class GrapheneSimulation
{
public:
	GrapheneSimulation();
	~GrapheneSimulation();

	int getParticleCount();
	void getPositionsCopy (std::vector<float>& containerForPositionsCopy);

	class Listener
	{
	public:
		Listener() = default;
		virtual ~Listener() = default;
		virtual void simulationUpdate (const std::vector<float> newPositions) = 0;
	};
	void addListener (GrapheneSimulation::Listener* listenerToAdd);
	void removeListener (GrapheneSimulation::Listener* listenerToRemove);

private:
	NarupaClient narupaClient;
	std::mutex positionsLock;
	std::vector<float> positions;

	std::mutex listenerLock;
	ListenerList<GrapheneSimulation::Listener> listeners;

	JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (GrapheneSimulation)
};
