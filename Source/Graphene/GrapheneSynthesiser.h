#pragma once

#include "../../JuceLibraryCode/JuceHeader.h"
#include "GrapheneSimulation.h"

class GrapheneSynthesiser : private GrapheneSimulation::Listener
{
public:
	GrapheneSynthesiser();
	~GrapheneSynthesiser();
	void getPositionsCopy(std::vector<float>& containerForPositionsCopy);
	void getWavetableCopy(std::vector<float>& containerForWavetableCopy);
	const std::array<float, 3>& getCentroid();

	void prepareToPlay(double newSampleRate, int maximumExpectedSamplesPerBlock);
	void releaseResources();
	void processBlock(AudioBuffer<float>& buffer, MidiBuffer& midiMessages);

private:
	void calculateCentroid();
	void calculateInitialScanPathDistances();
	//GrapheneSimulation::Listener
	void simulationUpdate(const std::vector<float> newPositions) override;
	void setFrequency(float newFrequency);

	GrapheneSimulation grapheneSimulation;
	float sampleRate		{ 44100.f };

	std::vector<float> initialPositions;
	std::vector<int> scanPathIndices;
	std::vector<float> scanPathInitialCentroidDistances;
	std::array<float, 3> centroid{ 0.f, 0.f, 0.f };

	std::vector<float> wavetableOne;
	std::vector<float> wavetableTwo;

	std::atomic<std::vector<float>*> wavetablePointer{ nullptr };
	std::atomic<std::vector<float>*> updatePointer{ nullptr };

	std::mutex wavetableForPlotLock;
	std::vector<float> wavetableForPlot;

	int scanPathSize { 0 };
	float elementsPerSample { 0.f };
	float elementPosition { 0.f };
};
