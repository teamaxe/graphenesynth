#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "GrapheneSynthesiser.h"

class GrapheneComponent :	public	Component,
							private Timer
{
public:
	GrapheneComponent (GrapheneSynthesiser& gs);
	~GrapheneComponent();

	void paint(Graphics&) override;

private:
	GrapheneComponent() = default;
	enum Axis
	{
		X,
		Y,
		Z
	};

	int coordinateToPixel(Axis axis, float value) const;
	void timerCallback() override;

	GrapheneSynthesiser& grapheneSynthesiser;

	std::array<Range<float>, 3> ranges;
	std::vector<float> positions;
};

class WavetableComponent :	public Component,
							private Timer
{
public:
	WavetableComponent(GrapheneSynthesiser& gs);
	~WavetableComponent();

	void paint(Graphics&) override;
private:
	WavetableComponent() = default;
	void timerCallback() override;

	GrapheneSynthesiser& grapheneSynthesiser;
	std::vector<float> wavetable;
};