#include "GrapheneSynthesiser.h"
#define _USE_MATH_DEFINES
#include <math.h>

GrapheneSynthesiser::GrapheneSynthesiser()
{
	scanPathIndices = GraphenePath::getPath(5);
	scanPathSize = scanPathIndices.size();

	Timer::callAfterDelay (1000, [this]() 
	{
		auto particleCount = grapheneSimulation.getParticleCount();
		jassert(particleCount > 0);
		grapheneSimulation.getPositionsCopy (initialPositions);
		calculateCentroid();
		calculateInitialScanPathDistances();
		wavetableOne.resize (scanPathIndices.size(), 0.f);
		wavetableTwo.resize (scanPathIndices.size(), 0.f);
		wavetablePointer = &wavetableOne;
		updatePointer = &wavetableTwo;

		wavetableForPlot.resize (scanPathIndices.size(), 0.f);
		
		grapheneSimulation.addListener(this);
	});
	
}

GrapheneSynthesiser::~GrapheneSynthesiser()
{
	grapheneSimulation.removeListener(this);
}

void GrapheneSynthesiser::getPositionsCopy(std::vector<float>& containerForPositionsCopy)
{
	grapheneSimulation.getPositionsCopy(containerForPositionsCopy);
}

void GrapheneSynthesiser::getWavetableCopy(std::vector<float>& containerForWavetableCopy)
{
	std::lock_guard<std::mutex> lg(wavetableForPlotLock);
	containerForWavetableCopy = wavetableForPlot;
}

const std::array<float, 3>& GrapheneSynthesiser::getCentroid()
{
	return centroid;
}

void GrapheneSynthesiser::prepareToPlay(double newSampleRate, int maximumExpectedSamplesPerBlock)
{
	sampleRate = newSampleRate;
	setFrequency(220.0f);

}

void GrapheneSynthesiser::releaseResources()
{

}

void GrapheneSynthesiser::processBlock(AudioBuffer<float>& buffer, MidiBuffer& midiMessages)
{
	//silence before the wavetable is loaded
	if (wavetablePointer.load() == nullptr)
	{
		for (auto i = 0; i < buffer.getNumChannels(); ++i)
			buffer.clear (i, 0, buffer.getNumSamples());
		return;
	}

	MidiBuffer::Iterator midiIterator(midiMessages);
	MidiMessage midiMessage;
	int midiMessagePosition;
	bool noteReady = midiIterator.getNextEvent (midiMessage, midiMessagePosition);

	for (int sampleCounter = 0; sampleCounter < buffer.getNumSamples(); sampleCounter++)
	{
		if (noteReady && sampleCounter == midiMessagePosition)
		{
			if (midiMessage.isNoteOn())
				setFrequency (MidiMessage::getMidiNoteInHertz(midiMessage.getNoteNumber()));
			noteReady = midiIterator.getNextEvent(midiMessage, midiMessagePosition);
		}

		auto firstElement = static_cast<int> (elementPosition);
		auto secondElement = firstElement + 1;
		if (secondElement >= scanPathSize)
			secondElement -= scanPathSize;

		auto firstCoeficient = 1.f - (elementPosition - static_cast<float>(firstElement));
		auto secondCoificient = 1.f - firstCoeficient;

		auto sample = firstCoeficient * (*wavetablePointer.load())[firstElement] + secondCoificient * (*wavetablePointer.load())[secondElement];
			
		elementPosition += elementsPerSample;
		if (elementPosition >= static_cast<float>(scanPathSize))
			elementPosition -= static_cast<float>(scanPathSize);

		for (int channel = 0; channel < buffer.getNumChannels(); ++channel)
		{
			auto* channelData = buffer.getWritePointer(channel, sampleCounter);
			*channelData = sample;
		}
	}
}

void GrapheneSynthesiser::calculateCentroid()
{
	for (auto index : scanPathIndices)
	{
		auto offset = index * 3;
		centroid[0] += initialPositions[offset];
		centroid[1] += initialPositions[offset + 1];
		centroid[2] += initialPositions[offset + 2];
	}
	centroid[0] /= scanPathIndices.size();
	centroid[1] /= scanPathIndices.size();
	centroid[2] /= scanPathIndices.size();
}

void GrapheneSynthesiser::calculateInitialScanPathDistances()
{
	scanPathInitialCentroidDistances.resize (scanPathIndices.size(), 0.f);
	int counter = 0;
	for (auto index : scanPathIndices)
	{
		auto offset = index * 3;
		auto xdist = initialPositions[offset + 0] - centroid[0];
		auto ydist = initialPositions[offset + 1] - centroid[1];
		auto zdist = initialPositions[offset + 2] - centroid[2];

		scanPathInitialCentroidDistances[counter++] = std::sqrtf (xdist * xdist + ydist * ydist + zdist * zdist);
	}
	jassert (counter == scanPathIndices.size());
}

void GrapheneSynthesiser::simulationUpdate(const std::vector<float> newPositions)
{
	//calculate displacement into update wavetable
	int counter = 0;
	for (auto index : scanPathIndices)
	{
		auto offset = index * 3;
		auto xdist = newPositions[offset + 0] - centroid[0];
		auto ydist = newPositions[offset + 1] - centroid[1];
		auto zdist = newPositions[offset + 2] - centroid[2];
		auto centroidDistance = std::sqrtf(xdist * xdist + ydist * ydist + zdist * zdist);
		(*updatePointer.load())[counter] = centroidDistance - scanPathInitialCentroidDistances[counter];
		counter++;
	}
	jassert(counter == scanPathIndices.size());
	{
		std::lock_guard<std::mutex> lg(wavetableForPlotLock);
		wavetableForPlot = *updatePointer.load();
	}
	updatePointer = wavetablePointer.exchange(updatePointer);
}

void GrapheneSynthesiser::setFrequency (float newFrequency)
{
	elementsPerSample = newFrequency * scanPathSize / sampleRate;
}
