#include "GrapheneSimulation.h"

std::vector<int> GraphenePath::getPath(int pathIndex)
{
	//must be in this range
	jassert (pathIndex > 0 && pathIndex <= 5);
	switch (pathIndex)
	{
	case 1: default: return { 339, 341, 378, 379, 380, 381 };
	case 2: return { 299, 301, 336, 337, 338, 340, 342, 343, 376, 377, 382, 383, 416, 418, 419, 420, 421, 422 };
	case 3: return { 259, 261, 295, 296, 297, 298, 300, 302, 303, 305, 334, 335, 344, 345, 374, 375, 384, 385, 414, 415, 417, 423, 424, 425, 456, 458, 459, 460, 461, 462 };
	case 4: return { 219, 221, 255, 256, 258, 260, 262, 263, 265, 292, 293, 294, 304, 306, 307, 332, 333, 346, 347, 372, 373, 386, 387, 412, 413, 426, 427, 452, 454, 455, 457, 463, 464, 465, 466, 496, 498, 499, 500, 501, 502 };
	case 5: return { 541, 540, 542, 503, 505, 504, 506, 467, 469, 468, 429, 428, 389, 388, 349, 348, 309, 308, 269, 267, 266, 264, 225, 223, 222, 220, 181, 179, 218, 216, 217, 215, 254, 252, 253, 251, 290, 291, 330, 331, 370, 371, 410, 411, 450, 451, 453, 492, 494, 495, 497, 536, 538, 539};
	}
	
}

GrapheneSimulation::GrapheneSimulation()
{
	narupaClient.setPositionCallback([this](std::vector<float> newPositions)
	{
		for (auto p : newPositions)
		{
			{
				std::lock_guard<std::mutex> lg(positionsLock);
				positions = newPositions;
			}
			std::lock_guard<std::mutex> lg(listenerLock);
			listeners.call(&Listener::simulationUpdate, newPositions);
		}
	});
}

GrapheneSimulation::~GrapheneSimulation()
{

}

int GrapheneSimulation::getParticleCount()
{
	std::lock_guard<std::mutex> lg(positionsLock);
	return positions.size() / 3;
}

void GrapheneSimulation::getPositionsCopy(std::vector<float>& containerForPositionsCopy)
{
	std::lock_guard<std::mutex> lg(positionsLock);
	containerForPositionsCopy = positions;
}

void GrapheneSimulation::addListener(GrapheneSimulation::Listener* listenerToAdd)
{
	std::lock_guard<std::mutex> lg(listenerLock);
	listeners.add (listenerToAdd);
}

void GrapheneSimulation::removeListener(GrapheneSimulation::Listener* listenerToRemove)
{
	std::lock_guard<std::mutex> lg(listenerLock);
	listeners.remove (listenerToRemove);
}
