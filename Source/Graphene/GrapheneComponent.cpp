#include "GrapheneComponent.h"

GrapheneComponent::GrapheneComponent (GrapheneSynthesiser& gs) :		grapheneSynthesiser (gs)
{
	startTimerHz(20);
}

GrapheneComponent::~GrapheneComponent()
{

}

void GrapheneComponent::paint(Graphics& g)
{
	// (Our component is opaque, so we must completely fill the background with a solid colour)
	g.fillAll(getLookAndFeel().findColour(ResizableWindow::backgroundColourId));
	g.setColour(Colours::white);

	if (!ranges[0].isEmpty())
	{
		const auto diameter = getWidth() / 42;
		juce::Rectangle<int> atomArea({ 0, 0, diameter, diameter });
		auto coordinateCounter = 0;
		while (coordinateCounter < positions.size())
		{
			auto x = positions[coordinateCounter++];
			auto y = positions[coordinateCounter++];
			auto z = positions[coordinateCounter++];

			auto xcentre = coordinateToPixel (X, x);
			auto ycentre = coordinateToPixel (Y, y);

			auto rect = atomArea.withCentre ({ coordinateToPixel(X, x), coordinateToPixel(Y,y) }).toFloat();

			g.fillEllipse(rect);
		}

		g.setColour(Colours::hotpink);
		for (auto index : GraphenePath::getPath(1))
		{
			auto offset = index * 3;
			auto x = positions[offset++];
			auto y = positions[offset++];
			auto z = positions[offset];

			auto xcentre = coordinateToPixel (X, x);
			auto ycentre = coordinateToPixel (Y, y);

			auto rect = atomArea.withCentre ({ coordinateToPixel(X, x), coordinateToPixel(Y,y) }).toFloat();

			g.fillEllipse(rect);
		}

		g.setColour(Colours::lightgreen);
		for (auto index : GraphenePath::getPath(5))
		{
			auto offset = index * 3;
			auto x = positions[offset++];
			auto y = positions[offset++];
			auto z = positions[offset];

			auto xcentre = coordinateToPixel(X, x);
			auto ycentre = coordinateToPixel(Y, y);

			auto rect = atomArea.withCentre({ coordinateToPixel(X, x), coordinateToPixel(Y,y) }).toFloat();

			g.fillEllipse(rect);
		}
		g.setColour(Colours::red);
		{
			const auto centroid = grapheneSynthesiser.getCentroid();
			auto x = centroid[0];
			auto y = centroid[1];
			auto z = centroid[2];

			auto xcentre = coordinateToPixel(X, x);
			auto ycentre = coordinateToPixel(Y, y);

			auto rect = atomArea.withCentre({ coordinateToPixel(X, x), coordinateToPixel(Y,y) }).toFloat();

			g.fillEllipse(rect);
		}
	}
	else if (positions.size() > 0)
	{
		auto axisCounter = 0;
		ranges[X] = Range<float>(positions[X], positions[X]);
		ranges[Y] = Range<float>(positions[Y], positions[Y]);
		ranges[Z] = Range<float>(positions[Z], positions[Z]);

		for (auto coordinate : positions)
		{
			auto& range = ranges[axisCounter];
			if (coordinate > range.getEnd())
				range.setEnd(coordinate);
			else if (coordinate < range.getStart())
				range.setStart(coordinate);

			if (++axisCounter > 2)
				axisCounter = 0;
		}
	}
}

int GrapheneComponent::coordinateToPixel(Axis axis, float value) const
{
	auto& range = axis == X ? ranges[X] : axis == Y ? ranges[Y] : ranges[Z];

	auto pixelLength = axis == X ? getWidth() - 50 : getHeight() - 50;

	float scaleFactor = pixelLength / range.getLength();

	auto position = (value - range.getStart()) * scaleFactor + 25;

	if (axis == Y)
		return getHeight() - position;
	
	return position;

}

void GrapheneComponent::timerCallback()
{
	grapheneSynthesiser.getPositionsCopy (positions);
	repaint();
}


WavetableComponent::WavetableComponent (GrapheneSynthesiser& synthesiser) : grapheneSynthesiser (synthesiser)
{
	startTimerHz(20);

}

WavetableComponent::~WavetableComponent()
{

}

void WavetableComponent::paint (Graphics& g)
{
	if (wavetable.size() == 0)
		return;

	g.setColour(Colours::white);

	const auto width = static_cast<float> (getWidth());

	const auto minValue = -1.f; //plot min and max values
	const auto maxValue = 1.f;
	const auto yValueScaleFactor = static_cast<float>(getHeight()) / (maxValue - minValue);

	const auto pixelsPerElement = width / static_cast<float>(wavetable.size() - 1);

	for (int i = 0; i < wavetable.size() - 1; i++)
	{
		const auto startValue = wavetable[i];
		const auto endValue = wavetable[i + 1];

		const auto ystart = (startValue + minValue) * -yValueScaleFactor;
		const auto yend = (endValue + minValue) * -yValueScaleFactor;

		const auto xstart = i * pixelsPerElement;
		const auto xend = (i + 1) * pixelsPerElement;

		g.drawLine ({xstart,ystart,xend,yend});  
	}
}

void WavetableComponent::timerCallback()
{
	grapheneSynthesiser.getWavetableCopy (wavetable);
	repaint();
}


