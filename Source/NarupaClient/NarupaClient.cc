/*
 *
 * Copyright 2015 gRPC authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "NarupaClient.h"

const std::string NarupaClient::Keys::bondPairs         = {"bond.pairs"};
const std::string NarupaClient::Keys::chainCount        = {"chain.count"};
const std::string NarupaClient::Keys::chainNames        = {"chain.names"};
const std::string NarupaClient::Keys::energyKinetic     = {"energy.kinetic"};
const std::string NarupaClient::Keys::particleCount     = {"particle.count"};
const std::string NarupaClient::Keys::particleElements  = {"particle.elements"};
const std::string NarupaClient::Keys::particleNames     = {"particle.names"};
const std::string NarupaClient::Keys::particlePositions = {"particle.positions"};
const std::string NarupaClient::Keys::particleResidues  = {"particle.residues"};
const std::string NarupaClient::Keys::residueChains     = {"residue.chains"};
const std::string NarupaClient::Keys::residueCount      = {"residue.count"};
const std::string NarupaClient::Keys::residueIds        = {"residue.ids"};
const std::string NarupaClient::Keys::systemBoxVectors  = {"system.box.vectors"};