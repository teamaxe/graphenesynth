// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: narupa/protocol/trajectory/frame.proto

#ifndef PROTOBUF_INCLUDED_narupa_2fprotocol_2ftrajectory_2fframe_2eproto
#define PROTOBUF_INCLUDED_narupa_2fprotocol_2ftrajectory_2fframe_2eproto

#include <limits>
#include <string>

#include <google/protobuf/port_def.inc>
#if PROTOBUF_VERSION < 3007000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers. Please update
#error your headers.
#endif
#if 3007000 < PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers. Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/port_undef.inc>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_table_driven.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/inlined_string_field.h>
#include <google/protobuf/metadata.h>
#include <google/protobuf/message.h>
#include <google/protobuf/repeated_field.h>  // IWYU pragma: export
#include <google/protobuf/extension_set.h>  // IWYU pragma: export
#include <google/protobuf/map.h>  // IWYU pragma: export
#include <google/protobuf/map_entry.h>
#include <google/protobuf/map_field_inl.h>
#include <google/protobuf/unknown_field_set.h>
#include "../array.pb.h"
#include <google/protobuf/struct.pb.h>
// @@protoc_insertion_point(includes)
#include <google/protobuf/port_def.inc>
#define PROTOBUF_INTERNAL_EXPORT_narupa_2fprotocol_2ftrajectory_2fframe_2eproto

// Internal implementation detail -- do not use these members.
struct TableStruct_narupa_2fprotocol_2ftrajectory_2fframe_2eproto {
  static const ::google::protobuf::internal::ParseTableField entries[]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::google::protobuf::internal::AuxillaryParseTableField aux[]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::google::protobuf::internal::ParseTable schema[3]
    PROTOBUF_SECTION_VARIABLE(protodesc_cold);
  static const ::google::protobuf::internal::FieldMetadata field_metadata[];
  static const ::google::protobuf::internal::SerializationTable serialization_table[];
  static const ::google::protobuf::uint32 offsets[];
};
void AddDescriptors_narupa_2fprotocol_2ftrajectory_2fframe_2eproto();
namespace narupa {
namespace protocol {
namespace trajectory {
class FrameData;
class FrameDataDefaultTypeInternal;
extern FrameDataDefaultTypeInternal _FrameData_default_instance_;
class FrameData_ArraysEntry_DoNotUse;
class FrameData_ArraysEntry_DoNotUseDefaultTypeInternal;
extern FrameData_ArraysEntry_DoNotUseDefaultTypeInternal _FrameData_ArraysEntry_DoNotUse_default_instance_;
class FrameData_ValuesEntry_DoNotUse;
class FrameData_ValuesEntry_DoNotUseDefaultTypeInternal;
extern FrameData_ValuesEntry_DoNotUseDefaultTypeInternal _FrameData_ValuesEntry_DoNotUse_default_instance_;
}  // namespace trajectory
}  // namespace protocol
}  // namespace narupa
namespace google {
namespace protobuf {
template<> ::narupa::protocol::trajectory::FrameData* Arena::CreateMaybeMessage<::narupa::protocol::trajectory::FrameData>(Arena*);
template<> ::narupa::protocol::trajectory::FrameData_ArraysEntry_DoNotUse* Arena::CreateMaybeMessage<::narupa::protocol::trajectory::FrameData_ArraysEntry_DoNotUse>(Arena*);
template<> ::narupa::protocol::trajectory::FrameData_ValuesEntry_DoNotUse* Arena::CreateMaybeMessage<::narupa::protocol::trajectory::FrameData_ValuesEntry_DoNotUse>(Arena*);
}  // namespace protobuf
}  // namespace google
namespace narupa {
namespace protocol {
namespace trajectory {

// ===================================================================

class FrameData_ValuesEntry_DoNotUse : public ::google::protobuf::internal::MapEntry<FrameData_ValuesEntry_DoNotUse, 
    ::std::string, ::google::protobuf::Value,
    ::google::protobuf::internal::WireFormatLite::TYPE_STRING,
    ::google::protobuf::internal::WireFormatLite::TYPE_MESSAGE,
    0 > {
public:
#if GOOGLE_PROTOBUF_ENABLE_EXPERIMENTAL_PARSER
static bool _ParseMap(const char* begin, const char* end, void* object, ::google::protobuf::internal::ParseContext* ctx);
#endif  // GOOGLE_PROTOBUF_ENABLE_EXPERIMENTAL_PARSER
  typedef ::google::protobuf::internal::MapEntry<FrameData_ValuesEntry_DoNotUse, 
    ::std::string, ::google::protobuf::Value,
    ::google::protobuf::internal::WireFormatLite::TYPE_STRING,
    ::google::protobuf::internal::WireFormatLite::TYPE_MESSAGE,
    0 > SuperType;
  FrameData_ValuesEntry_DoNotUse();
  FrameData_ValuesEntry_DoNotUse(::google::protobuf::Arena* arena);
  void MergeFrom(const FrameData_ValuesEntry_DoNotUse& other);
  static const FrameData_ValuesEntry_DoNotUse* internal_default_instance() { return reinterpret_cast<const FrameData_ValuesEntry_DoNotUse*>(&_FrameData_ValuesEntry_DoNotUse_default_instance_); }
  void MergeFrom(const ::google::protobuf::Message& other) final;
  ::google::protobuf::Metadata GetMetadata() const;
};

// -------------------------------------------------------------------

class FrameData_ArraysEntry_DoNotUse : public ::google::protobuf::internal::MapEntry<FrameData_ArraysEntry_DoNotUse, 
    ::std::string, ::narupa::protocol::ValueArray,
    ::google::protobuf::internal::WireFormatLite::TYPE_STRING,
    ::google::protobuf::internal::WireFormatLite::TYPE_MESSAGE,
    0 > {
public:
#if GOOGLE_PROTOBUF_ENABLE_EXPERIMENTAL_PARSER
static bool _ParseMap(const char* begin, const char* end, void* object, ::google::protobuf::internal::ParseContext* ctx);
#endif  // GOOGLE_PROTOBUF_ENABLE_EXPERIMENTAL_PARSER
  typedef ::google::protobuf::internal::MapEntry<FrameData_ArraysEntry_DoNotUse, 
    ::std::string, ::narupa::protocol::ValueArray,
    ::google::protobuf::internal::WireFormatLite::TYPE_STRING,
    ::google::protobuf::internal::WireFormatLite::TYPE_MESSAGE,
    0 > SuperType;
  FrameData_ArraysEntry_DoNotUse();
  FrameData_ArraysEntry_DoNotUse(::google::protobuf::Arena* arena);
  void MergeFrom(const FrameData_ArraysEntry_DoNotUse& other);
  static const FrameData_ArraysEntry_DoNotUse* internal_default_instance() { return reinterpret_cast<const FrameData_ArraysEntry_DoNotUse*>(&_FrameData_ArraysEntry_DoNotUse_default_instance_); }
  void MergeFrom(const ::google::protobuf::Message& other) final;
  ::google::protobuf::Metadata GetMetadata() const;
};

// -------------------------------------------------------------------

class FrameData final :
    public ::google::protobuf::Message /* @@protoc_insertion_point(class_definition:narupa.protocol.trajectory.FrameData) */ {
 public:
  FrameData();
  virtual ~FrameData();

  FrameData(const FrameData& from);

  inline FrameData& operator=(const FrameData& from) {
    CopyFrom(from);
    return *this;
  }
  #if LANG_CXX11
  FrameData(FrameData&& from) noexcept
    : FrameData() {
    *this = ::std::move(from);
  }

  inline FrameData& operator=(FrameData&& from) noexcept {
    if (GetArenaNoVirtual() == from.GetArenaNoVirtual()) {
      if (this != &from) InternalSwap(&from);
    } else {
      CopyFrom(from);
    }
    return *this;
  }
  #endif
  static const ::google::protobuf::Descriptor* descriptor() {
    return default_instance().GetDescriptor();
  }
  static const FrameData& default_instance();

  static void InitAsDefaultInstance();  // FOR INTERNAL USE ONLY
  static inline const FrameData* internal_default_instance() {
    return reinterpret_cast<const FrameData*>(
               &_FrameData_default_instance_);
  }
  static constexpr int kIndexInFileMessages =
    2;

  void Swap(FrameData* other);
  friend void swap(FrameData& a, FrameData& b) {
    a.Swap(&b);
  }

  // implements Message ----------------------------------------------

  inline FrameData* New() const final {
    return CreateMaybeMessage<FrameData>(nullptr);
  }

  FrameData* New(::google::protobuf::Arena* arena) const final {
    return CreateMaybeMessage<FrameData>(arena);
  }
  void CopyFrom(const ::google::protobuf::Message& from) final;
  void MergeFrom(const ::google::protobuf::Message& from) final;
  void CopyFrom(const FrameData& from);
  void MergeFrom(const FrameData& from);
  PROTOBUF_ATTRIBUTE_REINITIALIZES void Clear() final;
  bool IsInitialized() const final;

  size_t ByteSizeLong() const final;
  #if GOOGLE_PROTOBUF_ENABLE_EXPERIMENTAL_PARSER
  static const char* _InternalParse(const char* begin, const char* end, void* object, ::google::protobuf::internal::ParseContext* ctx);
  ::google::protobuf::internal::ParseFunc _ParseFunc() const final { return _InternalParse; }
  #else
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input) final;
  #endif  // GOOGLE_PROTOBUF_ENABLE_EXPERIMENTAL_PARSER
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const final;
  ::google::protobuf::uint8* InternalSerializeWithCachedSizesToArray(
      ::google::protobuf::uint8* target) const final;
  int GetCachedSize() const final { return _cached_size_.Get(); }

  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const final;
  void InternalSwap(FrameData* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return nullptr;
  }
  inline void* MaybeArenaPtr() const {
    return nullptr;
  }
  public:

  ::google::protobuf::Metadata GetMetadata() const final;

  // nested types ----------------------------------------------------


  // accessors -------------------------------------------------------

  // map<string, .google.protobuf.Value> values = 1;
  int values_size() const;
  void clear_values();
  static const int kValuesFieldNumber = 1;
  const ::google::protobuf::Map< ::std::string, ::google::protobuf::Value >&
      values() const;
  ::google::protobuf::Map< ::std::string, ::google::protobuf::Value >*
      mutable_values();

  // map<string, .narupa.protocol.ValueArray> arrays = 2;
  int arrays_size() const;
  void clear_arrays();
  static const int kArraysFieldNumber = 2;
  const ::google::protobuf::Map< ::std::string, ::narupa::protocol::ValueArray >&
      arrays() const;
  ::google::protobuf::Map< ::std::string, ::narupa::protocol::ValueArray >*
      mutable_arrays();

  // @@protoc_insertion_point(class_scope:narupa.protocol.trajectory.FrameData)
 private:
  class HasBitSetters;

  ::google::protobuf::internal::InternalMetadataWithArena _internal_metadata_;
  ::google::protobuf::internal::MapField<
      FrameData_ValuesEntry_DoNotUse,
      ::std::string, ::google::protobuf::Value,
      ::google::protobuf::internal::WireFormatLite::TYPE_STRING,
      ::google::protobuf::internal::WireFormatLite::TYPE_MESSAGE,
      0 > values_;
  ::google::protobuf::internal::MapField<
      FrameData_ArraysEntry_DoNotUse,
      ::std::string, ::narupa::protocol::ValueArray,
      ::google::protobuf::internal::WireFormatLite::TYPE_STRING,
      ::google::protobuf::internal::WireFormatLite::TYPE_MESSAGE,
      0 > arrays_;
  mutable ::google::protobuf::internal::CachedSize _cached_size_;
  friend struct ::TableStruct_narupa_2fprotocol_2ftrajectory_2fframe_2eproto;
};
// ===================================================================


// ===================================================================

#ifdef __GNUC__
  #pragma GCC diagnostic push
  #pragma GCC diagnostic ignored "-Wstrict-aliasing"
#endif  // __GNUC__
// -------------------------------------------------------------------

// -------------------------------------------------------------------

// FrameData

// map<string, .google.protobuf.Value> values = 1;
inline int FrameData::values_size() const {
  return values_.size();
}
inline const ::google::protobuf::Map< ::std::string, ::google::protobuf::Value >&
FrameData::values() const {
  // @@protoc_insertion_point(field_map:narupa.protocol.trajectory.FrameData.values)
  return values_.GetMap();
}
inline ::google::protobuf::Map< ::std::string, ::google::protobuf::Value >*
FrameData::mutable_values() {
  // @@protoc_insertion_point(field_mutable_map:narupa.protocol.trajectory.FrameData.values)
  return values_.MutableMap();
}

// map<string, .narupa.protocol.ValueArray> arrays = 2;
inline int FrameData::arrays_size() const {
  return arrays_.size();
}
inline const ::google::protobuf::Map< ::std::string, ::narupa::protocol::ValueArray >&
FrameData::arrays() const {
  // @@protoc_insertion_point(field_map:narupa.protocol.trajectory.FrameData.arrays)
  return arrays_.GetMap();
}
inline ::google::protobuf::Map< ::std::string, ::narupa::protocol::ValueArray >*
FrameData::mutable_arrays() {
  // @@protoc_insertion_point(field_mutable_map:narupa.protocol.trajectory.FrameData.arrays)
  return arrays_.MutableMap();
}

#ifdef __GNUC__
  #pragma GCC diagnostic pop
#endif  // __GNUC__
// -------------------------------------------------------------------

// -------------------------------------------------------------------


// @@protoc_insertion_point(namespace_scope)

}  // namespace trajectory
}  // namespace protocol
}  // namespace narupa

// @@protoc_insertion_point(global_scope)

#include <google/protobuf/port_undef.inc>
#endif  // PROTOBUF_INCLUDED_narupa_2fprotocol_2ftrajectory_2fframe_2eproto
