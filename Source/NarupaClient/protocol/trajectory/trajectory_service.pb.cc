// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: narupa/protocol/trajectory/trajectory_service.proto

#include "trajectory_service.pb.h"

#include <algorithm>

#include <google/protobuf/stubs/common.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/wire_format_lite_inl.h>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/reflection_ops.h>
#include <google/protobuf/wire_format.h>
// @@protoc_insertion_point(includes)
#include <google/protobuf/port_def.inc>

namespace narupa {
namespace protocol {
namespace trajectory {
}  // namespace trajectory
}  // namespace protocol
}  // namespace narupa
void InitDefaults_narupa_2fprotocol_2ftrajectory_2ftrajectory_5fservice_2eproto() {
}

constexpr ::google::protobuf::Metadata* file_level_metadata_narupa_2fprotocol_2ftrajectory_2ftrajectory_5fservice_2eproto = nullptr;
constexpr ::google::protobuf::EnumDescriptor const** file_level_enum_descriptors_narupa_2fprotocol_2ftrajectory_2ftrajectory_5fservice_2eproto = nullptr;
constexpr ::google::protobuf::ServiceDescriptor const** file_level_service_descriptors_narupa_2fprotocol_2ftrajectory_2ftrajectory_5fservice_2eproto = nullptr;
const ::google::protobuf::uint32 TableStruct_narupa_2fprotocol_2ftrajectory_2ftrajectory_5fservice_2eproto::offsets[1] = {};
static constexpr ::google::protobuf::internal::MigrationSchema* schemas = nullptr;
static constexpr ::google::protobuf::Message* const* file_default_instances = nullptr;

::google::protobuf::internal::AssignDescriptorsTable assign_descriptors_table_narupa_2fprotocol_2ftrajectory_2ftrajectory_5fservice_2eproto = {
  {}, AddDescriptors_narupa_2fprotocol_2ftrajectory_2ftrajectory_5fservice_2eproto, "narupa/protocol/trajectory/trajectory_service.proto", schemas,
  file_default_instances, TableStruct_narupa_2fprotocol_2ftrajectory_2ftrajectory_5fservice_2eproto::offsets,
  file_level_metadata_narupa_2fprotocol_2ftrajectory_2ftrajectory_5fservice_2eproto, 0, file_level_enum_descriptors_narupa_2fprotocol_2ftrajectory_2ftrajectory_5fservice_2eproto, file_level_service_descriptors_narupa_2fprotocol_2ftrajectory_2ftrajectory_5fservice_2eproto,
};

const char descriptor_table_protodef_narupa_2fprotocol_2ftrajectory_2ftrajectory_5fservice_2eproto[] =
  "\n3narupa/protocol/trajectory/trajectory_"
  "service.proto\022\032narupa.protocol.trajector"
  "y\032*narupa/protocol/trajectory/get_frame."
  "proto2\312\003\n\021TrajectoryService\022n\n\017Subscribe"
  "Frames\022+.narupa.protocol.trajectory.GetF"
  "rameRequest\032,.narupa.protocol.trajectory"
  ".GetFrameResponse0\001\022t\n\025SubscribeLatestFr"
  "ames\022+.narupa.protocol.trajectory.GetFra"
  "meRequest\032,.narupa.protocol.trajectory.G"
  "etFrameResponse0\001\022h\n\tGetFrames\022+.narupa."
  "protocol.trajectory.GetFrameRequest\032,.na"
  "rupa.protocol.trajectory.GetFrameRespons"
  "e0\001\022e\n\010GetFrame\022+.narupa.protocol.trajec"
  "tory.GetFrameRequest\032,.narupa.protocol.t"
  "rajectory.GetFrameResponseb\006proto3"
  ;
::google::protobuf::internal::DescriptorTable descriptor_table_narupa_2fprotocol_2ftrajectory_2ftrajectory_5fservice_2eproto = {
  false, InitDefaults_narupa_2fprotocol_2ftrajectory_2ftrajectory_5fservice_2eproto, 
  descriptor_table_protodef_narupa_2fprotocol_2ftrajectory_2ftrajectory_5fservice_2eproto,
  "narupa/protocol/trajectory/trajectory_service.proto", &assign_descriptors_table_narupa_2fprotocol_2ftrajectory_2ftrajectory_5fservice_2eproto, 594,
};

void AddDescriptors_narupa_2fprotocol_2ftrajectory_2ftrajectory_5fservice_2eproto() {
  static constexpr ::google::protobuf::internal::InitFunc deps[1] =
  {
    ::AddDescriptors_narupa_2fprotocol_2ftrajectory_2fget_5fframe_2eproto,
  };
 ::google::protobuf::internal::AddDescriptors(&descriptor_table_narupa_2fprotocol_2ftrajectory_2ftrajectory_5fservice_2eproto, deps, 1);
}

// Force running AddDescriptors() at dynamic initialization time.
static bool dynamic_init_dummy_narupa_2fprotocol_2ftrajectory_2ftrajectory_5fservice_2eproto = []() { AddDescriptors_narupa_2fprotocol_2ftrajectory_2ftrajectory_5fservice_2eproto(); return true; }();
namespace narupa {
namespace protocol {
namespace trajectory {

// @@protoc_insertion_point(namespace_scope)
}  // namespace trajectory
}  // namespace protocol
}  // namespace narupa
namespace google {
namespace protobuf {
}  // namespace protobuf
}  // namespace google

// @@protoc_insertion_point(global_scope)
#include <google/protobuf/port_undef.inc>
