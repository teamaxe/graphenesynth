/*
 *
 * Copyright 2015 gRPC authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
#pragma once

#include <iostream>
#include <memory>
#include <string>
#include <thread>

#include <grpcpp/grpcpp.h>

#include "protocol/trajectory/trajectory_service.grpc.pb.h"

using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;

class NarupaClient
{
public:
    NarupaClient()
            :   stub_(narupa::protocol::trajectory::TrajectoryService::NewStub(grpc::CreateChannel(
            "localhost:54321", grpc::InsecureChannelCredentials()))),
                thread (&NarupaClient::run, this)


    {

    }

    struct SimulationState
    {
    public:
        void setParticleCount (int newNumberOfParticles)
        {
            if (numberOfParticles != newNumberOfParticles)
            {
                numberOfParticles  = newNumberOfParticles;
                positions.resize(numberOfParticles, 0.f);
            }
        }
        int                 getParticleCount() const    { return numberOfParticles; }
        std::vector<float>&  getPositionsReference()    { return positions; }
        bool                isPopulated() const         { return numberOfParticles != 0; }
    private:
        int numberOfParticles {0};
        std::vector<float> positions;
        //std::vector<std::tuple<float,float,float>> positions;
    };

    ~NarupaClient()
    {
        stop();
        thread.join();
        std::cout << "Thread joined" << std::endl;
    }
    void stop()
    {
        shouldExit = true;
    }

    void setPositionCallback (std::function<void(std::vector<float>)> callback)
    {
        std::lock_guard<std::mutex> locker (callbacksLock);
        onPositionUpdate = std::move (callback);
    }

private:
    struct Keys
    {
        static const std::string bondPairs;         //array<uint32> pairs of bonded atoms
        static const std::string chainCount;        //int number of individual chains
        static const std::string chainNames;        //array<strings> name of above chains
        static const std::string energyKinetic;     //float system's kinetic energy
        static const std::string particleCount;     //float number atoms in the system
        static const std::string particleElements;  //array<uint32> index in the peroidic table
        static const std::string particleNames;     //array<string> array of all atom names
        static const std::string particlePositions; //array<float> position of each atom
        static const std::string particleResidues;  //array<uint32> residue to which each atom belongs
        static const std::string residueChains;     //array<uint32>
        static const std::string residueCount;      //
        static const std::string residueIds;        //array<strings> names of residues
        static const std::string systemBoxVectors;  //array<float> 9 - unit cell vector for the constraints
    };

    void run()
    {
        subscribeToFrames();
    }
    void subscribeToFrames()
    {
        ClientContext context;
        narupa::protocol::trajectory::GetFrameResponse frameResponse;

        std::unique_ptr<grpc::ClientReader<narupa::protocol::trajectory::GetFrameResponse> > reader(
                stub_->SubscribeLatestFrames(&context, narupa::protocol::trajectory::GetFrameRequest()));
        while (reader->Read(&frameResponse) && shouldExit == false)
        {
            //std::cout << "Frame" << frameResponse.frame_index() << std::endl;// << " positions: " << frameResponse.frame.arrays['particle.positions']
            auto& frame = frameResponse.frame();

//            for (auto d : frame.arrays())
//                std::cout << "arrays:" << d.first << std::endl;
//            for (auto d : frame.values())
//                std::cout << "values" << d.first << std::endl;

            auto particleCount = frame.values().find (Keys::particleCount);
            if (particleCount != frame.values().end())
            {
                auto particleCountpt = *particleCount;
                state.setParticleCount (particleCount->second.number_value());
                std::cout << particleCount->first << " : " << particleCount->second.kind_case() << "Number:" << state.getParticleCount() << std::endl;
            }

//            auto value = frame.values().find (Keys::particleElements);
//
//            if ( value != frame.values().end())
//            {
//                auto valuept = *value;
//                state.setParticleCount (particleCount->second.number_value());
//                //std::cout << particleCount->first << " : " << particleCount->second.kind_case() << "Number:" << state.getParticleCount() << std::endl;
//            }

            auto particlePositionsIt = frame.arrays().find (Keys::particlePositions);

            if (particlePositionsIt != frame.arrays().end())
            {
                auto particlePositions = particlePositionsIt->second.float_values().values();
                state.getPositionsReference().assign(particlePositions.begin(), particlePositions.end());
                std::lock_guard<std::mutex> locker (callbacksLock);
                if (onPositionUpdate)
                {
                    onPositionUpdate (state.getPositionsReference());
                }
            }

        }
        Status status = reader->Finish();

    }

    std::unique_ptr<narupa::protocol::trajectory::TrajectoryService::Stub> stub_;
    std::thread thread;
    std::atomic<bool> shouldExit {false};

    SimulationState state;

    std::mutex callbacksLock;
    std::function<void(std::vector<float>)> onPositionUpdate;
};

