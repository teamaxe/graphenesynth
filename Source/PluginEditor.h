/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "PluginProcessor.h"
#include "Graphene/GrapheneComponent.h"

//==============================================================================
/**
*/
class AudioClientAudioProcessorEditor  :	public	AudioProcessorEditor
{
public:
    AudioClientAudioProcessorEditor (AudioClientAudioProcessor&);
    ~AudioClientAudioProcessorEditor();

    //==============================================================================
    void paint (Graphics&) override;
    void resized() override;

private:
    AudioClientAudioProcessor& processor;
	GrapheneComponent grapheneComponent;
	WavetableComponent wavetableComponent;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (AudioClientAudioProcessorEditor)
};
