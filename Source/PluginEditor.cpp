/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
AudioClientAudioProcessorEditor::AudioClientAudioProcessorEditor (AudioClientAudioProcessor& p)
	:	AudioProcessorEditor (&p), 
		processor (p), 
		grapheneComponent (processor.getGrapheneSynthesiser()),
		wavetableComponent (processor.getGrapheneSynthesiser())

{
	addAndMakeVisible (grapheneComponent);
	addAndMakeVisible (wavetableComponent);
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    setSize (300, 400);
}

AudioClientAudioProcessorEditor::~AudioClientAudioProcessorEditor()
{
}

//==============================================================================
void AudioClientAudioProcessorEditor::paint (Graphics& g)
{

}

void AudioClientAudioProcessorEditor::resized()
{
    // This is generally where you'll want to lay out the positions of any
    // subcomponents in your editor..
	juce::Rectangle<int> r (getLocalBounds());
	grapheneComponent.setBounds (r.removeFromTop(getWidth()));
	wavetableComponent.setBounds (r);
}
